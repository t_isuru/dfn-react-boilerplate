import { combineReducers } from 'redux'
import exampleReducer1 from './ExampleReducer1'
import exampleReducer2 from './ExampleReducer2'
import authReducer from "./AuthReducer";

// Combines our two example reducers to create a root reducer
// reducers can be accessed as stste.reducer1, state.reducer2 after connecting the state to props
export default combineReducers({
  reducer1: exampleReducer1,
  reducer2: exampleReducer2,
  authReducer: authReducer
})
