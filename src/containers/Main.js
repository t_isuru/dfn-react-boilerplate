import { connect } from 'react-redux'
import MainView from "../components/MainView";
import {login} from "../actions";


const mapStateToProps = state => ({
    items: state.reducer1,
    token: state.authReducer
});

const mapDispatchToProps = (dispatch) => ({
    loginClick: () => dispatch(login())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MainView)