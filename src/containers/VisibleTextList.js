import { connect } from 'react-redux'
import { toggleRow } from '../actions'
import TextList from '../components/TextList'
import { VisibilityFilters } from '../actions'

const getVisibleTextList = (txtList, filter) => {
  switch (filter) {
    case VisibilityFilters.SHOW_ALL:
      return txtList;
    case VisibilityFilters.SHOW_COMPLETED:
      return txtList.filter(t => t.strikeThrough);
    case VisibilityFilters.SHOW_ACTIVE:
      return txtList.filter(t => !t.strikeThrough);
    default:
      throw new Error('Unknown filter: ' + filter)
  }
};

const mapStateToProps = state => ({
  items: getVisibleTextList(state.reducer1, state.reducer2)
});

const mapDispatchToProps = dispatch => ({
  toggleRow: id => dispatch(toggleRow(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TextList)
