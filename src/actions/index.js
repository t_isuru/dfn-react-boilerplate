let nextId = 0;
export const addText = text => ({
  type: 'ADD_TEXT',
  id: nextId++,
  text
});

export const setVisibilityFilter = filter => ({
  type: 'SET_VISIBILITY_FILTER',
  filter
});

export const toggleRow = id => ({
  type: 'TOGGLE',
  id
});

export const login = () => ({
  type: 'LOGIN'
});

export const VisibilityFilters = {
  SHOW_ALL: 'SHOW_ALL',
  SHOW_COMPLETED: 'SHOW_COMPLETED',
  SHOW_ACTIVE: 'SHOW_ACTIVE'
};
