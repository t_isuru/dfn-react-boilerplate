import React from 'react'
import PropTypes from 'prop-types'
import TextRow from './Text'

const TextList = ({ items, toggleRow }) => (
  <ul>
    {items.map(item =>
      <TextRow
        key={item.id}
        {...item} // ... ES6 notation for spreading elements to a new Set of elements.
          // Ex: https://www.codingame.com/playgrounds/7998/es6-tutorials-spread-operator-with-fun
        onClick={() => toggleRow(item.id)}
      />
    )}
  </ul>
);

// TodoList.propTypes = {
//   todos: PropTypes.arrayOf(PropTypes.shape({
//     id: PropTypes.number.isRequired,
//     completed: PropTypes.bool.isRequired,
//     text: PropTypes.string.isRequired
//   }).isRequired).isRequired,
//   toggleTodo: PropTypes.func.isRequired
// };

export default TextList
