const exampleReducer1 = (state = [], action) => {
  switch (action.type) {
    case 'ADD_TEXT':
      return [
        ...state,
        {
          id: action.id,
          text: action.text,
          strikeThrough: false
        }
      ];
    case 'TOGGLE':
      return state.map(text =>
        (text.id === action.id)
          ? {...text, strikeThrough: !text.strikeThrough}
          : text
      );
    default:
      return state
  }
};

export default exampleReducer1
