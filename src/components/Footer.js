import React from 'react'
import FilterButton from '../containers/FilterButton'
import { VisibilityFilters } from '../actions'

const Footer = () => (
  <div>
    <span>Show: </span>
    <FilterButton filter={VisibilityFilters.SHOW_ALL}>
      All
    </FilterButton>
    <FilterButton filter={VisibilityFilters.SHOW_ACTIVE}>
      Plain
    </FilterButton>
    <FilterButton filter={VisibilityFilters.SHOW_COMPLETED}>
      Strike Through
    </FilterButton>
  </div>
);

export default Footer
